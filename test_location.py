# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestLocation():
  def setup_method(self, method):
    self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_location(self):
    self.driver.get("https://yandex.ru/")
    self.driver.set_window_size(945, 1020)
    self.driver.find_element(By.CSS_SELECTOR, ".geolink__reg").click()
    self.driver.find_element(By.ID, "city__front-input").click()
    self.driver.find_element(By.ID, "city__front-input").send_keys("Владивосток")
    self.driver.find_element(By.CSS_SELECTOR, ".b-autocomplete-item__reg").click()
    element = self.driver.find_element(By.CSS_SELECTOR, ".traffic__icon")
    actions = ActionChains(self.driver)
    actions.move_to_element(element).perform()
    element = self.driver.find_element(By.CSS_SELECTOR, "body")
    actions = ActionChains(self.driver)
    actions.move_to_element(element, 0, 0).perform()
  
